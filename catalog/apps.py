from django.apps import AppConfig

#arquivo de configuração de exibição das apps no admin

class CatalogConfig(AppConfig):
    name = 'catalog'
    verbose_name ='Catálogo'
