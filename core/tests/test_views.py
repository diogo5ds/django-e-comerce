from django.test import TestCase, Client
from django.core.urlresolvers import reverse


class IndexViewTestCase(TestCase):

    def setUp(self):
        self.client = Client()
        self.url = reverse('index.html')

    def test_status_code(self):
        response = self.client.get(self.html)
        self.assertEquals(response.status_code, 200)

    def test_template_use(self):
        response = self.client.get(self.url)
        self.assertTemplateUsed(response,'index.html')
